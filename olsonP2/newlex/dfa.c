#include "dfa.h"
#include <string.h>

Filein filein;
extern FILE * lst; //This pointer refers to a FILE pointer declared in syn.c


char * token_names [] = {"ERROR", "IDENT", "NUMLIT", "PLUS", 
			 "MINUS", "MULT", "DIV", "MOD", "EXP", "NOT", "AND", "OR", 
			 "XOR", "ASSIGN", "LT", "GT", "SHIFTL", "SHIFTR", "PLUSPLUS", 
			 "PLUSEQ", "MINUSMINUS", "MINUSEQ", "MULTEQ", "DIVEQ", "MODEQ", 
			 "EXPEQ", "NOTEQ", "LOGAND", "ANDEQ", "LOGOR", "OREQ", "XOREQ", 
			 "EQUALTO", "SHIFTLEQ", "LTE", "SHIFTREQ", "GTE", "TILDE", "RPAREN",
			 "LPAREN", "SEMI", "COMMA", "QUEST", "COLON", "ERROR", "NUMLIT", 
			 "INTTYPE", "DBLTYPE", "EOFT"};

valid_t reduce_char(char c);

int get_special_type(char * id)
{
  char * i = "int";
  char * d = "double";
  if(strcmp(id, i) == 0)
    return 43;
  else if(strcmp(id, d) == 0)
    return 44;
  return -2;
}

char* concat(char *str, char c)
{
  size_t len = strlen(str);
  char *updatedstr = malloc( len + 2 );
  strcpy(updatedstr, str);
  updatedstr[len] = c;
  updatedstr[len+1] = '\0';
  return updatedstr;
}

size_t stringLen(const char *str)
{
  const char *s;
  for(s = str; *s; ++s);
  return(s - str);
}

char * get_lexeme()
{ 
  return filein.lex;
}

void reportID_error()
{
  filein.errorcount++;
  fprintf(lst, "Lexical error found on line %d: Invalid Identifier found: %s\n", filein.linecount, filein.lex);
}

void report_error()
{
  filein.errorcount++;
  fprintf(lst, "Lexical error at %d,%d: Invalid character found: %c\n", filein.linecount, filein.position, *filein.lex);
}

token get_next_state(token current, valid_t c)
{
//a 0, 1 1, + 2, - 3, * 4, % 5, / 6, = 7, < 8, > 9, ! 10, & 11, | 12, ^ 13, ~ 14, . 15, ( 16, ) 17, ; 18, , 19, ? 20, : 21, inv 22
  int state_table[46][25] = {{1, 2, 3, 4, 5, 7, 6, 13, 14, 15, 9, 10, 11, 12, 37, 44, 39, 38, 40, 45, 45, 41, 42, 43, 0},            //START 0
			     {1, 1, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},    //IDENT 1
			     {45, 2, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 44, 45, 45, 45, 45, 45, ?, ?, ?, 45},    //NUMLIT 2
			     {45, 45, 18, 45, 45, 45, 45, 19, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //PLUS   3
			     {45, 45, 45, 20, 45, 45, 45, 21, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //MINUS  4
			     {45, 45, 45, 45, 8, 45, 45, 22, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //MULT 5
			     {45, 45, 45, 45, 45, 45, 45, 23, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //DIV 6
			     {45, 45, 45, 45, 45, 45, 45, 24, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //MOD 7
			     {45, 45, 45, 45, 45, 45, 45, 25, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},   //EXP 8
			     {45, 45, 45, 45, 45, 45, 45, 26, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //NOT 9
 			     {45, 45, 45, 45, 45, 45, 45, 28, 45, 45, 45, 27, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //AND 10
			     {45, 45, 45, 45, 45, 45, 45, 30, 45, 45, 45, 45, 29, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //OR 11
			     {45, 45, 45, 45, 45, 45, 45, 31, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //XOR 12 
			     {45, 45, 45, 45, 45, 45, 45, 32, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //ASSIGN 13
			     {45, 45, 45, 45, 45, 45, 45, 34, 16, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //LT 14
			     {45, 45, 45, 45, 45, 45, 45, 36, 45, 17, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //GT 15
			     {45, 45, 45, 45, 45, 45, 45, 33, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //SHIFTL 16
			     {45, 45, 45, 45, 45, 45, 45, 35, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //SHIFTR 17
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //PLUSPLUS 18
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //PLUSEQ 19
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //MINUSMINUS 20
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //MINUSEQ 21
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //MULTEQ 22
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //DIVEQ 23
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //MODEQ 24
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //EXPEQ 25
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //NOTEQ 26
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //LOGAND 27
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //ANDEQ 28
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //LOGOR 29
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //OREQ 30
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //XOREQ 31
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //EQUALTO 32
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //SHIFTLEQ 33
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //LTE 34
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //SHIFTREQ 35
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //GTE 36
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //TILDE 37
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //RPAREN 38
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //LPAREN 39
			     {45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45},  //SEMI 40
			     (), // COMMA 41
			     (), // QUEST 42
			     (), // COLON 43
			     {45, 42, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, ?, ?, ?, 45}, //NUMNUT 44
			     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};  //ERROR 45
  
  
  token ret = (state_table[current][c]);
  //  printf("the return value = %d\n", ret);
  return ret;

}
void init (char * filename)
{
  filein.input = fopen(filename, "r");
  filein.position = 0;
  filein.linecount = 1;
  filein.errorcount = 0;
  filein.lex = malloc(sizeof(char *));
  filein.read = getline(&filein.line, &filein.len, filein.input);
  fprintf(lst, "%d. %s",filein.linecount, filein.line);
}

char read_character()
{
  char c = filein.line[filein.position];
  if(c == '\n'){
    filein.read = getline(&filein.line, &filein.len, filein.input);
    filein.position = 0;
    filein.linecount ++;
    if(filein.read > 0){
      c = filein.line[filein.position];
      fprintf(lst, "%d. %s",filein.linecount, filein.line);
    }
    else{
      return 0;
    }
  }
  filein.position++;
  filein.nc = reduce_char(filein.line[filein.position]);
  return c;
}

token get_token(){
  filein.lex = "";
  token cs = ERROR;
  token ns = ERROR;
  token ks = ERROR;
  valid_t cc;
  char c = read_character();
    
  while(ns != 45){
    if(c == '\0'){
      return EOFT;
    }
    cc = (reduce_char(c));
    ns = (get_next_state(cs, cc));
    ks = (get_next_state(ns, filein.nc));
    if(cc != ws){
      filein.lex = concat(filein.lex, c);
      if(cc == invalid){
	report_error();
	return 0;
      }
      if(ks == 45){
	if(cc == dot && strlen(filein.lex) < 2){
	  report_error();
	  return 0;
	}
	else if(cs == IDENT && strlen(filein.lex) > 32){
	  reportID_error();
	  return 0;
	}
	else if(cs == IDENT && get_special_type(filein.lex) >= 43){
	  return get_special_type(filein.lex);
	}
	else if(cs == IDENT && strlen(filein.lex) < 2 && cc != ws)
	  return cs;
	else
	  return ns;
      }
      else{
	if(ns != ERROR)
	  cs = ns;
      }
    }
    c = read_character();
  }
  
}

valid_t reduce_char(char c)
{
  if(c > 126 || c < 0)
    return invalid;
  
  static valid_t lookup[256] = {
    eof,          //0
    invalid,      //1
    invalid,      //2
    invalid,      //3
    invalid,      //4
    invalid,      //5
    invalid,      //6
    invalid,      //7
    invalid,      //8
    ws,           //9
    ws,           //10
    ws,           //11
    ws,           //12
    ws,           //13
    invalid,      //14
    invalid,      //15
    invalid,      //16
    invalid,      //17
    invalid,      //18
    invalid,      //19
    invalid,      //20
    invalid,      //21
    invalid,      //22
    invalid,      //23
    invalid,      //24
    invalid,      //25
    invalid,      //26
    invalid,      //27
    invalid,      //28
    invalid,      //29
    invalid,      //30
    invalid,      //31
    ws,           //32
    bang,         //33
    invalid,      //34
    invalid,      //35
    invalid,      //36
    mod,          //37
    amp,          //38
    invalid,      //39
    rpar,         //40
    lpar,         //41
    mult,         //42
    pls,          //43
    invalid,      //44
    mns,          //45
    dot,          //46
    divide,       //47
    digit,        //48
    digit,        //49
    digit,        //50
    digit,        //51
    digit,        //52
    digit,        //53
    digit,        //54
    digit,        //55
    digit,        //56
    digit,        //57
    invalid,      //58
    semi,         //59
    lt,           //60
    eql,          //61
    gt,           //62
    invalid,      //63
    invalid,      //64
    alpha,        //65
    alpha,        //66
    alpha,        //67
    alpha,        //68
    alpha,        //69
    alpha,        //70
    alpha,        //71
    alpha,        //72
    alpha,        //73
    alpha,        //74
    alpha,        //75
    alpha,        //76
    alpha,        //77
    alpha,        //78
    alpha,        //79
    alpha,        //80
    alpha,        //81
    alpha,        //82
    alpha,        //83
    alpha,        //84
    alpha,        //85
    alpha,        //86
    alpha,        //87
    alpha,        //88
    alpha,        //89
    alpha,        //90
    invalid,      //91
    invalid,      //92
    invalid,      //93
    carrot,       //94
    alpha,        //95
    invalid,      //96
    alpha,        //97
    alpha,        //98
    alpha,        //99
    alpha,        //100
    alpha,        //101
    alpha,        //102
    alpha,        //103
    alpha,        //104
    alpha,        //105
    alpha,        //106
    alpha,        //107
    alpha,        //108
    alpha,        //109
    alpha,        //110
    alpha,        //111
    alpha,        //112
    alpha,        //113
    alpha,        //114
    alpha,        //115
    alpha,        //116
    alpha,        //117
    alpha,        //118
    alpha,        //119
    alpha,        //120
    alpha,        //121
    alpha,        //122
    invalid,      //123
    pipe,         //124
    invalid,      //125
    tilde,        //126
  };
  return lookup[c];
}

void end_lex(){
  fprintf(lst, "\n%d lexical errors found in input file\n", filein.errorcount);
  fclose(filein.input);
  return;
}


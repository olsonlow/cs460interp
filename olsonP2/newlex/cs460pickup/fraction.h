// Assignment : Homework 02
// File : fraction.h
// Author : Dr. Watts

// Description : This is the undocumented header file for the
// fraction class

#ifndef FRACTION_H
#define FRACTION_H

// Include input and output stream and string libraries.
#include <iostream>

// Use the standard namespace.
using namespace std;

class fraction
{
public:

	fraction ();
	fraction (long long w, long long n, unsigned long long d);
	bool operator < (const fraction & other) const;
	fraction operator + (const fraction & other) const;
	fraction operator += (const fraction & other);
	friend istream & operator >> (istream & ins, fraction & f);
	friend ostream & operator << (ostream & outs, const fraction & f);

private:
	void reduce ();

	long long numerator;
	unsigned long long denominator;
};

#endif

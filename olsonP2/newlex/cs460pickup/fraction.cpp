// Assignment : Homework 02
// File : fraction.cpp
// Author : Dr. Watts

// Description : This is the undocumented implementation file for the
// fraction class

#include "fraction.h"

// Use the standard namespace.
using namespace std;

fraction::fraction ()
{
	numerator = 0;
	denominator = 1;
}

fraction::fraction (long long w, long long n, unsigned long long d)
{
	numerator = d * w + n;
	denominator = d;
	this->reduce();
}

bool fraction::operator < (const fraction & other) const
{
	long long p1 = numerator * other.denominator;
	long long p2 = other.numerator * denominator;
	return p1 < p2;
}

fraction fraction::operator + (const fraction & other) const
{
	long long N = numerator * other.denominator + other.numerator * denominator;
	unsigned long long D = denominator * other.denominator;
	return fraction (0, N, D);
}

fraction fraction::operator += (const fraction & other)
{
	*this = *this + other;
	return *this;
}

istream & operator >> (istream & ins, fraction & f)
{	

	long long W, N;
	unsigned long long D;
	char temp;

	ins >> W >> N >> temp >> D;
	if (ins.fail())
		return ins;
	f.numerator = W * (long long) D + (W < 0 ? -N : N);
	f.denominator = (long long) D;
	f.reduce();

	return ins;
}

ostream & operator << (ostream & outs, const fraction & f)
{	
	long long W = f.numerator / (int) f.denominator;
	long long N = f.numerator % (int) f.denominator;

	outs << W << ' ' << (W < 0 ? -N : N) << '/' << f.denominator;
	
	return outs;
}

void fraction::reduce ()
{
	long long f = 2;
	bool neg = false;
	
	if (numerator < 0)
	{
		neg = true;
		numerator = -numerator;
	}
	while (f <= denominator && f <= numerator)
		if ((numerator % f == 0) && (denominator % f == 0))
		{
			numerator /= f;
			denominator /= f;
		}
		else 
			f++;
	if (neg)
		numerator = -numerator;
	if (numerator == 0)
		denominator = 1;
}
